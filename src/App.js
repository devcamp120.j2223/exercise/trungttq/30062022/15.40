import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from "reactstrap"
import { FormComponent } from './components/Form';

function App() {
  return (
    <Container className="bg-light border mt-5">
      <FormComponent/>
    </Container>
  );
}

export default App;
