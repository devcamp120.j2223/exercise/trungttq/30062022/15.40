import {
  Form,
  Row,
  Col,
  FormGroup,
  Label,
  InputGroup,
  Input,
  Button,
  CardImg,
} from "reactstrap";

export function FormComponent() {
  return (
    <Form>
      <Col className="text-center mt-2">
        <h3>REGISTRATION FORM</h3>
      </Col>
      <Row>
        <Col md={6}>
          <Row>
            <Col sm={3} className="mt-3">
              <Label>
                First Name <span className="text-danger">(*)</span>
              </Label>
            </Col>
            <Col sm={9} className="mt-3">
              <Input placeholder="first name.." />
            </Col>
          </Row>
          <Row>
            <Col sm={3} className="mt-3">
              <Label>
                Last Name <span className="text-danger">(*)</span>
              </Label>
            </Col>
            <Col sm={9} className="mt-3">
              <Input placeholder="last name..." />
            </Col>
          </Row>
          <Row>
            <Col sm={3} className="mt-3">
              <Label>
                Birthday <span className="text-danger">(*)</span>
              </Label>
            </Col>
            <Col sm={9} className="mt-3">
              <Input placeholder="birthday..." />
            </Col>
          </Row>
          <Row>
            <Col sm={3} className="mt-3">
              <Label>Gender<span className="text-danger">(*)</span> </Label>
            </Col>
            <Col sm={9} className="mt-3">
                <Row>
                <Col>
                <Input type="radio" name="gender"/>
                <Label className="mx-2">Male</Label>
              </Col>
              <Col>
                <Input type="radio" name="gender" />
                <Label className="mx-2">Female</Label>
              </Col>
                </Row>
            </Col>
          </Row>
        </Col>
        <Col sm={6}>
          <Row>
            <Col sm={3} className="mt-3">
            <Label>Passport<span className="text-danger">(*)</span> </Label>
            </Col>
            <Col sm={9} className="mt-3">
              <Input placeholder="passport.." />
            </Col>
          </Row>
          <Row>
            <Col sm={3} className="mt-3">
            <Label>Email</Label>
            </Col>
            <Col sm={9} className="mt-3">
              <Input placeholder="email.." />
            </Col>
          </Row>
          <Row>
            <Col sm={3} className="mt-3">
            <Label>Country<span className="text-danger">(*)</span> </Label>
            </Col>
            <Col sm={9} className="mt-3">
              <Input placeholder="country.." />
            </Col>
          </Row>
          <Row>
            <Col sm={3} className="mt-3">
            <Label>Region</Label>
            </Col>
            <Col sm={9} className="mt-3">
              <Input placeholder="region.." />
            </Col>
          </Row>
        </Col>
          <Col sm={2} className="mt-2">
            <Label>Subject </Label>
          </Col>
          <Col sm={10} className="mt-2">
            <Input type="textarea" rows="4" />
          </Col>
      </Row>

      <Row className="mt-3">
        <Col sm={7}></Col>
        <Col sm={5}>
          <Button color="success" className="mx-2">
            Check Data
          </Button>
          <Button color="success" className="mx-2">
            Send
          </Button>
        </Col>
      </Row>
    </Form>
  );
}
